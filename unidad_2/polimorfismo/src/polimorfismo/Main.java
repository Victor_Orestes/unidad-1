package polimorfismo;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		B[] bs = new B[3];
		bs[0] = new A();
		bs[1] = new B();
		bs[2] = new C();
		
		naivePrinter(bs);
	}

	private static void naivePrinter(B[] bs) {
		// TODO Auto-generated method stub
		for(int i=0; i< bs.length;i++)
		{
			bs[i].print();
		}
	}

}
