package Paquete;

import javax.swing.JOptionPane;


public class Clase
{
	String captura;
	public void Mensaje()
	{
		JOptionPane.showMessageDialog(null, "Has invocado mensaje de la clase.");
	}
	public void Edad()
	{
		this.captura = JOptionPane.showInputDialog("�Qu� edad tienes?");
		JOptionPane.showConfirmDialog(null, "�En serio tienes "+captura+"?, te vez m�s joven.");
	}
	public String Confirmar()
	{
		return this.captura;
	}

}
class ClaseHijo extends Clase
{
	public ClaseHijo()
	{
		JOptionPane.showMessageDialog(null, "Estas en la clase heredada");
	}
	public void A�o(String edad)
	{
		int age;
		age = Integer.parseInt(edad);
		age = age + 2;
		JOptionPane.showMessageDialog(null, "Perdon. Mi edad real es "+age+" a�os.");
	}
}
class ClaseVecino extends Clase
{
	public ClaseVecino()
	{
		
	}
	public void A�o(String edad)
	{
		int age;
		age = Integer.parseInt(edad);
		age = age + 4;
		JOptionPane.showMessageDialog(null, "Pero mi vecino tiene "+age+" a�os, �l es m�s viejo que yo.");
	}
}
